function showLogin() {
    document.getElementById("action").innerText = "Sign-In";
    document.getElementById("login").style.display = "block";
    document.getElementById("register").style.display = "none";
    document.getElementById("toreg").className = "notsel";
    document.getElementById("tolog").className = "sel";
}
function showReg() {
    document.getElementById("action").innerText = "Register";
    document.getElementById("login").style.display = "none";
    document.getElementById("register").style.display = "block";
    document.getElementById("tolog").className = "notsel";
    document.getElementById("toreg").className = "sel";
}
function login(event) {
    const form = document.getElementById("login");
    if (form.reportValidity()) {
        event.preventDefault();

        const uname = document.getElementById("uname").value.trim();
        const pass = document.getElementById("pass").value.trim();

        const credentials = {
            uname: uname,
            pass: pass
        };

        const loginUser = async () => {
            const response = await fetch('http://localhost:3000/login', {
                method: 'POST',
                body: JSON.stringify(credentials),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            return await response.json();
        }

        const result = loginUser();

        result.then((data) => {
            if (data.flag == true) {
                signIn(data.data);
            } else {
                alert(data.message);
            }
            
        });
    }
}

function getAllData(uname) {
    const forName = {
        uname: uname
    };
    const getData = async () => {
        const response = await fetch('http://localhost:3000/getData', {
            method: 'POST',
            body: JSON.stringify(forName),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
        const result = await response.json();
        return result;
    }
    const result = getData();
    return result;
}

function signIn(data) {
    document.getElementById("maindiv").style.display = "none";
    document.getElementById("info").style.display = "block";
    document.getElementById("data").style.display = "block";
    document.getElementById(
        "action"
    ).innerText = `Login Successfull !! Welcome ${data.fname} ${data.lname}`;
    sessionStorage.setItem("currUser", JSON.stringify(data));

    document.getElementById("info_uname").innerText = data.uname;
    document.getElementById(
        "info_name"
    ).innerText = `${data.fname} ${data.lname}`;
    document.getElementById("info_email").innerText = data.email;
    document.getElementById("info_gender").innerText = data.gender;
    const element = document.createElement("p");
    element.className = "dataField";

    const promiseData = getAllData(data.uname);
    promiseData.then((dataFromPromise) => {
        console.log(dataFromPromise.message);
        if (dataFromPromise.flag) { 
            const allUserData = dataFromPromise.message;
            let html =
                "<table><th>Name</th><th>Email</th><th>Gender</th></tr>";
            for (let i = 0; i < allUserData.length; i++) {
                if (allUserData[i].uname == data.uname) {
                    continue;
                } else {
                    html =
                        html +
                        `<tr><td> ${allUserData[i].fname} ${allUserData[i].lname} </td><td> ${allUserData[i].email} </td><td> ${allUserData[i].gender} </td></tr>`;
                }
            }
            html += " </table>";
            element.innerHTML = html;
            document.getElementById("data").appendChild(element);
        }
    });

}

function register(event) {
    const form = document.getElementById("register");
    if (form.reportValidity()) {
        event.preventDefault();

        const fname = document.getElementById("fname").value.trim();
        const lname = document.getElementById("lname").value.trim();
        const email = document.getElementById("email").value.trim();
        const uname = document.getElementById("reg_uname").value.trim();
        const pass = document.getElementById("reg_pass").value.trim();
        const gender = document.getElementById("gender").value;

        const user = {
            fname: fname,
            lname: lname,
            email: email,
            uname: uname,
            pass: pass,
            gender: gender
        };

        const registerRequest = async () => {
            const response = await fetch('http://localhost:3000/register', {
                method: 'POST',
                body: JSON.stringify(user),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            return await response.json();
        }

        const result = registerRequest();

        result.then((data) => {
            alert(data.message);
            if (data.flag) {
                location.reload();
            }
        });

    }

}

function logout() {
    sessionStorage.removeItem("currUser");
    const credentials = {
        uname: uname
    };
    const logout = async () => {
        const response = await fetch('http://localhost:3000/logout', {
            method: 'POST',
            body: JSON.stringify(credentials),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
        return await response.json();
    }

    const result = logout();

    result.then((data) => {        
        location.reload();
    });
}
