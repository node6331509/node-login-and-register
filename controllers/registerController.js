import model from '../models/model.js';

function validate(req, res, next) {
  if (
    !req.body.fname
    || !req.body.lname
    || !req.body.uname
    || !req.body.email
    || !req.body.pass
    || !req.body.gender
  ) {
    return res.status(200).send({
      status: 200,
      message: 'Please send all the details',
    });
  } else {
    next();
  }
}

function checkExisting(req, res, next) {
  const uname = req.body.uname;
  const email = req.body.email;
  const data = model.getData();

  const checkEmail = data.find((user) => user.email === email);
  const checkUname = data.find((user) => user.uname === uname);
  if (data.length === 0) {
    next();
  } else if (checkEmail === undefined && checkUname === undefined) {
    next();
  } else if (checkEmail) {
    return res.status(200).send({
      status: 200,
      message: 'Email already exists',
      flag: false,
    });
  } else if (checkUname) {
    return res.status(200).send({
      status: 200,
      message: 'Username already exists',
      flag: false,
    });
  } else {
    return res.status(200).send({
      status: 200,
      message: 'Some check error',
      flag: false,
    });
  }
}

function register(req, res) {
  if (model.saveData(req.body)) {
    return res.status(200).send({
      status: 200,
      message: 'Successful registeration',
      flag: true,
    });
  } else {
    return res.status(200).send({
      status: 200,
      message: 'Can\'t Save',
      flag: false,
    });
  }
}

export default {
  register, validate, checkExisting,
};
