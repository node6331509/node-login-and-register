import model from '../models/model.js';

function validate(req, res, next) {
  if (!req.body.uname || !req.body.pass) {
    return res.status(200).send({
      status: 200,
      message: 'Please send both the details',
    });
  } else {
    next();
    return true;
  }
}

function createSession(req, res, uname) {
  req.session[uname] = true;
}

function login(req, res) {
  const { uname, pass } = req.body;

  const data = model.getData();
  const index = data.findIndex((ele) => ele.uname === uname);
  if (index === -1) {
    return res.status(200).send({
      status: 200,
      message: 'No user with this Username exists!',
      flag: false,
    });
  } else {
    if (data[index].pass === pass) {
      createSession(req, res, uname);
      return res.status(200).send({
        status: 200,
        message: 'Success',
        data: {
          fname: data[index].fname,
          lname: data[index].lname,
          uname,
          email: data[index].email,
          gender: data[index].gender,
        },
        flag: true,
      });
    }
    return res.status(200).send({
      status: 200,
      message: 'Wrong Password',
      flag: false,
    });
  }
}

function getData(req, res) {
  const data = model.getData();
  return res.status(200).send({
    status: 200,
    message: data,
    flag: true,
  });
}

function logout(req, res) {
  delete req.session[req.body.uname];
  return res.status(200).send({
    status: 200,
    message: 'logged out',
  });
}

function checkSession(req, res, next) {
  console.log('check:', req.session[req.body.uname]);
  if (!req.body.uname) {
    return res.status(200).send({
      status: 200,
      message: 'No username sent',
      flag: false,
    });
  }

  if (req.session[req.body.uname]) {
    next();
  } else {
    return res.status(200).send({
      status: 200,
      message: 'No Session Active',
      flag: false,
    });
  }
}

export default {
  validate, login, getData, checkSession, logout,
};
