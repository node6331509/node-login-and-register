import registerController from './controllers/registerController.js';
import loginController from './controllers/loginController.js';

function routes(app) {
  app.post(
    '/register',
    registerController.validate,
    registerController.checkExisting,
    registerController.register,
  );
  app.post('/login', loginController.validate, loginController.login);
  app.post('/getData', loginController.checkSession, loginController.getData);
  app.post('/logout', loginController.logout);
}

export default routes;
