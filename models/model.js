import fs from 'fs';

function saveData(data) {
  if (!fs.existsSync('./datafiles/data.json')) {
    const arr = [];
    arr.push(data);
    fs.writeFileSync('./datafiles/data.json', JSON.stringify(arr));
    return true;
  } else {
    const dataRaw = fs.readFileSync('./datafiles/data.json');
    const dataJson = JSON.parse(dataRaw);
    dataJson.push(data);
    fs.writeFileSync('./datafiles/data.json', JSON.stringify(dataJson));
    return true;
  }
}

function getData() {
  if (fs.existsSync('./datafiles/data.json')) {
    const dataRaw = fs.readFileSync('./datafiles/data.json');
    return JSON.parse(dataRaw);
  } else {
    return [];
  }
}

export default { saveData, getData };
