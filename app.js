import express from 'express';
import session from 'express-session';
import routes from './routes.js';

function middlewares(app) {
  app.use(express.static('./Client'));
  app.use(express.json());
  app.use(session({ secret: 'secretKey' }));

  app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', '*');
    next();
  });

  routes(app);
}

export default middlewares;
