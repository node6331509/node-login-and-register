import express from 'express';
import middlewares from './app.js';

const app = express();

middlewares(app);

app.listen(3000, () => {
  console.log('Server Listening on port 3000');
});
